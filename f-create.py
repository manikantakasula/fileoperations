#Function to create a file
def create_file (name,extension):
  #path which accepts variable as user specified name and extension
  path="C:/Users/mkasula/{0}{1}".format(name,extension)
  #opens the File with specified path
  #'w' - opens a file for writing . If the file doesn't exist it creates a new one
  f=open(path,'w')
  #Description to write inside a file
  f.write('created file')
  return "created a new file in {0}".format(path)




#calling with parameters  
print(create_file("mani",".txt"))